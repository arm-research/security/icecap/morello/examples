
purecap:
	@echo "Building purecap"
	/home/siagraw/cheri/output/morello-sdk/bin/clang   --target=aarch64-linux-gnu   main.c -march=morello+c64 -mabi=purecap -O0 -o main-purecap

mixed:
	@echo "Building mixed"
	/home/siagraw/cheri/output/morello-sdk/bin/clang   --target=aarch64-linux-gnu   main.c -march=morello -O0 -o main-mixed

regular:
	@echo "Building regular"
	/home/siagraw/cheri/output/morello-sdk/bin/clang   --target=aarch64-linux-gnu   main.c -O0 -o main-regular

clean:
	@echo "Cleaning"
	rm -rf main-purecap main-mixed main-regular a.out
